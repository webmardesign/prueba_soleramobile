export class Servicio {
  constructor(
    public id:number,
    public categoria:string,
    public titulo:string,
    public descripcion:string
  ){}
}