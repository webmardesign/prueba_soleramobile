const servicios = [
  {
    id: 1,
    categoria: 'Hogar',
    titulo: 'Electricidad',
    descripcion: 'Lorem ipsum, dolor sit amet.'
  },
  {
    id: 2,
    categoria: 'Autos',
    titulo: 'Auxilio Mecanico',
    descripcion: 'Lorem ipsum, dolor sit amet.'
  },
  {
    id: 3,
    categoria: 'Autos',
    titulo: 'Chofer remplazo',
    descripcion: 'Lorem ipsum, dolor sit amet.'
  },
  {
    id: 4,
    categoria: 'Autos',
    titulo: 'Chofer elegido',
    descripcion: 'Lorem ipsum, dolor sit amet.'
  },
  {
    id: 5,
    categoria: 'Salud',
    titulo: 'Medico a domicilio',
    descripcion: 'Lorem ipsum, dolor sit amet.'
  },
  {
    id: 6,
    categoria: 'Salud',
    titulo: 'Ambulancia',
    descripcion: 'Lorem ipsum, dolor sit amet.'
  },
  {
    id: 7,
    categoria: 'Hogar',
    titulo: 'Gasfitero',
    descripcion: 'Lorem ipsum, dolor sit amet.'
  },
  {
    id: 8,
    categoria: 'Hogar',
    titulo: 'Seguro contra robo de domicilio',
    descripcion: 'Lorem ipsum, dolor sit amet.'
  },
  {
    id: 9,
    categoria: 'Hogar',
    titulo: 'Decorador de interiores',
    descripcion: 'Lorem ipsum, dolor sit amet.'
  }
];

module.exports = { servicios };