const db = require('./db');

const filterServicesById = function(id){
  const result = (!id) ? db.servicios : db.servicios.filter((value) => value.id == id);
  return result;
}

const filterServices = () => db.servicios;

module.exports = { filterServicesById, filterServices };