const express = require('express');
const bodyParser = require('body-parser');
const db = require('./procesingData');
const port = process.env.PORT || 3000;
const app = express();

app.use(bodyParser.json({ type: 'application/json' }));
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
})

app.get('/services/:id?', (req, res) => {
  var id = req.params.id;
  res.json({'data': db.filterServicesById(id)});  
});

app.get('/services', (req, res) => {
  res.json({'data': db.filterServices()}); 
})

app.listen(port, () => {
  console.log('We are live on ' + port);
})