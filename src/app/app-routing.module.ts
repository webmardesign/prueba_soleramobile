import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Importar Componente principal
import { ListaServiciosComponent } from './components/lista-servicios/lista-servicios.component';
import { ListaTodosComponent } from './components/lista-todos/lista-todos.component';
import { ListaAutosComponent } from './components/lista-autos/lista-autos.component';
import { ListaSaludComponent } from './components/lista-salud/lista-salud.component';
import { ListaHogarComponent } from './components/lista-hogar/lista-hogar.component';

const appRoutes:Routes = [
  {
    path: '',
    component: ListaServiciosComponent,
    children: [
      {
        path: 'todos',
        // component: ListaTodosComponent,
        loadChildren: './components/lista-todos/lista-todos.module#ListaTodosModule',
        // children: [
        //   {
        //     path: 'servicio-detalle/:id',
        //     loadChildren: './components/servicio-detalle/servicio-detalle.module#ServicioDetalleModule'
        //   }
        // ]
      },
      {
        path: 'autos',
        // component: ListaAutosComponent,
        loadChildren: './components/lista-autos/lista-autos.module#ListaAutosModule',
        // children: [
        //   {
        //     path: 'servicio-detalle/:id',
        //     loadChildren: './components/servicio-detalle/servicio-detalle.module#ServicioDetalleModule'
        //   }
        // ]
      },
      {
        path: 'salud',
        // component: ListaSaludComponent,
        loadChildren: './components/lista-salud/lista-salud.module#ListaSaludModule',
        // children: [
        //   {
        //     path: 'servicio-detalle/:id',
        //     loadChildren: './components/servicio-detalle/servicio-detalle.module#ServicioDetalleModule'
        //   }
        // ]
      },
      {
        path: 'hogar',
        // component: ListaHogarComponent,
        loadChildren: './components/lista-hogar/lista-hogar.module#ListaHogarModule',
        // children: [
        //   {
        //     path: 'servicio-detalle/:id',
        //     loadChildren: './components/servicio-detalle/servicio-detalle.module#ServicioDetalleModule'
        //   }
        // ]
      },
      {
        path: 'servicio-detalle/:id',
        loadChildren: './components/servicio-detalle/servicio-detalle.module#ServicioDetalleModule'
      }
    ]
  },
  {path: '**', redirectTo: 'todos'}
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule],
  providers: []
})

export class AppRoutingModule {}