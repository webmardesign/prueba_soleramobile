import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListaTodosComponent } from './lista-todos.component';
import { Routes, RouterModule } from '@angular/router';

const routes:Routes = [{
  path: '',
  component: ListaTodosComponent
}];

@NgModule({
  declarations: [ListaTodosComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [],
  providers: [],
})
export class ListaTodosModule {}