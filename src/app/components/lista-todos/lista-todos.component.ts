import { Component, OnInit } from '@angular/core';
import { Servicio } from '../../models/servicio';
import { ServiciosService } from '../../services/servicios.service';

@Component({
  selector: 'lista-todos',
  templateUrl: './lista-todos.component.html',
  styleUrls: ['./lista-todos.component.scss'],
  providers: [ServiciosService]
})
export class ListaTodosComponent implements OnInit {

  public servicios:Array<Servicio>;

  constructor(private _peticion:ServiciosService) {
    this.servicios = [];
  }

  ngOnInit() {
    this.getServices();
  }

  getServices() {
    this._peticion.getServicios().subscribe(
			result => {
        this.servicios = result.data;
        console.log(this.servicios);
			},
			error => {
        console.log(<any>error);
			}
		);
  }

}
