import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListaAutosComponent } from './lista-autos.component';
import { Routes, RouterModule } from '@angular/router';

const routes:Routes = [{
  path: '',
  component: ListaAutosComponent
}];

@NgModule({
  declarations: [ListaAutosComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [],
  providers: [],
})
export class ListaAutosModule {}