import { Component, OnInit } from '@angular/core';
import { Servicio } from '../../models/servicio';
import { ServiciosService } from '../../services/servicios.service';

@Component({
  selector: 'app-lista-autos',
  templateUrl: './lista-autos.component.html',
  styleUrls: ['./lista-autos.component.scss'],
  providers: [ServiciosService]
})
export class ListaAutosComponent implements OnInit {

  public servicios:Array<Servicio>;

  constructor(private _peticion:ServiciosService) {
    this.servicios = [];
  }

  ngOnInit() {
    this._peticion.getServicios().subscribe(
			result => {
        this.servicios = result.data.filter(value => {
          return value.categoria === "Autos" || value.categoria === "autos";
        });
			},
			error => {
        console.log(<any>error);
			}
		);
  }

}
