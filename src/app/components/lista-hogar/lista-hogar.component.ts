import { Component, OnInit } from '@angular/core';
import { Servicio } from '../../models/servicio';
import { ServiciosService } from '../../services/servicios.service';

@Component({
  selector: 'app-lista-hogar',
  templateUrl: './lista-hogar.component.html',
  styleUrls: ['./lista-hogar.component.scss'],
  providers: [ServiciosService]
})
export class ListaHogarComponent implements OnInit {

  public servicios:Array<Servicio>;

  constructor(private _peticion:ServiciosService) {
    this.servicios = [];
  }

  ngOnInit() {
    this._peticion.getServicios().subscribe(
			result => {
        this.servicios = result.data.filter(value => {
          return value.categoria === "Hogar" || value.categoria === "hogar";
        });
			},
			error => {
				console.log(<any>error);
			}
		);
  }

}
