import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaHogarComponent } from './lista-hogar.component';

describe('ListaHogarComponent', () => {
  let component: ListaHogarComponent;
  let fixture: ComponentFixture<ListaHogarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaHogarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaHogarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
