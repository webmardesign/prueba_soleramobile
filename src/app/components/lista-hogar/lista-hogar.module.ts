import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListaHogarComponent } from './lista-hogar.component';
import { Routes, RouterModule } from '@angular/router';

const routes:Routes = [{
  path: '',
  component: ListaHogarComponent
}];

@NgModule({
  declarations: [ListaHogarComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [],
  providers: [],
})
export class ListaHogarModule {}