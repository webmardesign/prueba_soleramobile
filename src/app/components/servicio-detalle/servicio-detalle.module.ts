import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ServicioDetalleComponent } from './servicio-detalle.component';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const routes:Routes = [{
  path: '',
  component: ServicioDetalleComponent
}];

@NgModule({
  declarations: [ServicioDetalleComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ],
  exports: [],
  providers: [],
})
export class ServicioDetalleModule {}