import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Servicio } from '../../models/servicio';
import { ServiciosService } from '../../services/servicios.service';

@Component({
  selector: 'servicio-detalle',
  templateUrl: './servicio-detalle.component.html',
  styleUrls: ['./servicio-detalle.component.scss'],
  providers: [ServiciosService]
})
export class ServicioDetalleComponent implements OnInit {

  public title:string;
  public servicio:Servicio;

  constructor(
    private _peticion: ServiciosService,
    private _route: ActivatedRoute,
    private _router: Router
  ) {
    this.title = "Editar servicio";
    this.servicio = new Servicio(0,"","","");
  }

  ngOnInit() {
    this.getService();
  }

  getService() {
    this._route.params.forEach((params:Params) => {
      let id = params['id'];
      this._peticion.getServicio(id).subscribe(
        result => {
          this.servicio = result.data;
          console.log(this.servicio);
        },
        error => {
          console.log(<any>error);
        }
      );
    });
  }

  onSubmit() {
    console.log('Hola mundito');
  }

}
