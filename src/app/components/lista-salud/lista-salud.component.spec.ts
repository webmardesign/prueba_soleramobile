import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaSaludComponent } from './lista-salud.component';

describe('ListaSaludComponent', () => {
  let component: ListaSaludComponent;
  let fixture: ComponentFixture<ListaSaludComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaSaludComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaSaludComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
