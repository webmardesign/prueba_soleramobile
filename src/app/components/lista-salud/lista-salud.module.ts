import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListaSaludComponent } from './lista-salud.component';
import { Routes, RouterModule } from '@angular/router';

const routes:Routes = [{
  path: '',
  component: ListaSaludComponent
}];

@NgModule({
  declarations: [ListaSaludComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [],
  providers: [],
})
export class ListaSaludModule {}