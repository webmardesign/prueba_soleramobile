import { Component, OnInit } from '@angular/core';
import { Servicio } from '../../models/servicio';
import { ServiciosService } from '../../services/servicios.service';

@Component({
  selector: 'app-lista-salud',
  templateUrl: './lista-salud.component.html',
  styleUrls: ['./lista-salud.component.scss'],
  providers: [ServiciosService]
})
export class ListaSaludComponent implements OnInit {

  public servicios:Array<Servicio>;

  constructor(private _peticion:ServiciosService) {
    this.servicios = [];
  }

  ngOnInit() {
    this._peticion.getServicios().subscribe(
      result => {
        this.servicios = result.data.filter(value => {
          return value.categoria === "Salud" || value.categoria === "salud";
        });
      },
      error => {
        console.log(<any>error);
      }
    );
  }

}
