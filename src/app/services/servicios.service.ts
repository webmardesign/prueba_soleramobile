import {Injectable} from '@angular/core';
import { Http, Response, Headers, Request } from '@angular/http';
import { HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Global } from './global.service';

@Injectable()

export class ServiciosService {

	public url_servicios:string;
	public url:string;

	constructor(private _http:Http) {
		this.url = Global.url;
	}

	getServicios() {
		return this._http.get(this.url + 'services').map(res => res.json());
	}

	getServicio(id) {
		return this._http.get(this.url + 'services/' + id).map(res => res.json());
	}

	deleteServicio(id) {
		return this._http.get(this.url + 'delete-service/' + id).map(res => res.json());
	}

}